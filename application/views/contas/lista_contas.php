<div class="container">
    <div class="text-center mt-3">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Nova conta
        </a>
    </div>
    <div class="collapse" id="collapseExample">
        <div class="mt-1 mb-5">
            <div class="row">
                <div class="col-md-6 mx-auto border mt-3 pt-3 pb-3">
                    <form method="POST" id="contas-form">

                        <input class="form-control" name="parceiro" type="text" placeholder="Devedor / Credor"><br />
                        <input class="form-control" name="descricao" type="text" placeholder="Descrição"><br />
                        <input class="form-control" name="valor" type="number" placeholder="Valor"><br /><br />

                        <div class="row mb-4">
                            <div class="col-md-6">
                                <input class="form-control" name="mes" type="number" placeholder="Mês" />
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="Ano" type="number" placeholder="Ano" />
                            </div>
                        </div>

                        <input type="hidden" name="tipo" value="<?= $tipo ?>">

                        <div class="text-center text-md-left">
                            <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Enviar</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <hr style="background-color: black; width: 70%;" />

    <div class="row">
        <div class="col">
            <?= $lista ?>
        </div>
    </div>

</div>